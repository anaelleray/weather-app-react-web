import { Component } from "react";
import { Col, Container, Row, FormGroup } from "react-bootstrap";
import { connect } from "react-redux";
import { fetchInfos, onCityChangeHandler } from '../actions/weatherStackActions'
import Loader from "../Loader";
import './WeatherComponent.css'

class WeatherComponent extends Component {
    
    render() {
        if (!this.props.weather){
            return (
                <Loader />
            )
        }
        { this.props.fetchInfos() }

        return (
            <Container fluid id="mainContainer">
                <Row>
                    <Col>
                        <h1 id="title">{this.props.weather.Name}</h1>
                        <h3 id="date">{this.props.weather.Date}</h3>
                    </Col>
                </Row>
                <Row id="weather-infos">
                    <Row id="desc">
                        <Col>
                            <div>
                                <img src='./media/weather.png' alt=""></img>
                            </div>
                        </Col>
                        <Col>
                            <div>
                                <p id="weatherDesc">{this.props.weather.WeatherDesc}</p>
                            </div>
                        </Col>
                    </Row>
                    <Row id="temp">
                        <Col>
                            <div>
                                <p id="temperature">{this.props.weather.Temperature}°C</p>
                            </div>
                        </Col>
                        <Col>
                            <div id="minmax">
                                <p style={{ margin: 0 }}>Min -146°C</p>
                                <hr></hr>
                                <p style={{ margin: 0 }}>Max +84°C</p>
                            </div>
                        </Col>
                    </Row>
                </Row>
                <Row id="wind-hum">
                    <Col id="wind">
                        <img style={{ marginLeft: 10, marginRight: 10 }} src="./media/wind.png" alt="" title="somethin"></img>
                        <p style={{ fontWeight: "bold" }}>{this.props.weather.Wind} Km/h</p>
                    </Col>
                    <Col id="hum">
                        <img style={{ marginLeft: 30, marginRight: 10, height: 50 }} src="./media/double_drop.png" alt=""></img>
                        <p style={{ fontWeight: "bold" }}>{this.props.weather.Humidity} %</p>
                    </Col>
                </Row>
                <Row>
                    <FormGroup>
                        <Col id="selectCol">
                            <select className="select" onChange={this.props.onCityChangeHandler}>
                                <option value="Lyon" defaultValue>Selectionnez une ville</option>
                                <option value="Lyon">Lyon</option>
                                <option value="Paris">Paris</option>
                                <option value="Toulouse">Toulouse</option>
                                <option value="Marseille">Marseille</option>
                                <option value="Londres">Londres</option>
                                <option value="Berlin">Berlin</option>
                                <option value="Dublin">Dublin</option>
                                <option value="Galway">Galway</option>
                                <option value="Washington">Washington</option>
                                <option value="Budapest">Budapest</option>
                                <option value="Moscou">Moscou</option>
                                <option value="Tokyo">Tokyo</option>
                                <option value="Seoul">Seoul</option>
                            </select>
                        </Col>
                    </FormGroup>
                </Row>
            </Container>
        )
    }
}
const mapStateToProps = state => {
    return {
        weather: state.weather
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        fetchInfos: () => {
            dispatch(fetchInfos())
        },
        onCityChangeHandler: (event) => {
            dispatch(onCityChangeHandler(event))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(WeatherComponent)