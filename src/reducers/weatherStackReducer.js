const INITIAL_STATE = {
    weather: {
        Name: "",
        Date: "",
        Temperature: 0,
        WeatherDesc: "",
        Wind: 0,
        Humidity: 0,
        selectedCity: "Lyon"
    }
}
const weatherStackReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case "SET_DATA_FROM_CITY":
            state = {
                ...state,
                weather: action.payload
            }
            break;
        case "SELECTED_CITY":
            state = {
                ...state,
                weather: {
                    ...state.weather,
                    selectedCity: action.value
                }
            }  
            break;
        default:
    }
    return state;
}
export default weatherStackReducer;